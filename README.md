Sphero Spikes
=============

This is an attempt to make [Sphero](http://gosphero.com) roll in fun and unproductive ways.

This uses [Artoo](http://artoo.io). Follow instructions about Sphero's [platform](http://artoo.io/documentation/platforms/sphero/) and [driver](http://artoo.io/documentation/drivers/sphero/).
