require 'artoo'

connection :sphero, :adaptor => :sphero, :port => '127.0.0.1:8023'
device :sphero, :driver => :sphero

work do
  @direction = 0
  @speed = @minimum_speed = 70
  @colors = [:red, :green, :blue]
  @color = @colors[1]

  on sphero, collision: proc { |*args|
    @color = @colors[rand(3)]
    sphero.set_color(@color)
    @direction = rand(360)
    @speed = rand(100) + @minimum_speed
    puts "Wow. Such obstacle. Changing direction to #{@direction}, color to #{@color} and speed to #{@speed}."
  }

  every(1.seconds) do
    sphero.roll @speed, @direction
  end

  sphero.stop
end
